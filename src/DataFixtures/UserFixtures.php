<?php

namespace App\DataFixtures;

use App\Entity\Beekeeper;
use App\Entity\Individual;
use App\Enum\EnumGender;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $entityManagerInterface;
    private $userPasswordHasherInterface;

    public function __construct(
        EntityManagerInterface $entityManagerInterface,
        UserPasswordHasherInterface $userPasswordHasherInterface,
    ) {
        $this->entityManagerInterface = $entityManagerInterface;
        $this->userPasswordHasherInterface = $userPasswordHasherInterface;
        
    }

    public function load(ObjectManager $manager)
    {
        $this->loadIndividual($manager);
        $this->loadBeekeeper($manager);
    }

    /**
     * Create Individual
     *
     * @param ObjectManager $manager
     * @return void
     */
    private function loadIndividual(ObjectManager $manager): void
    {
        // utilisation de la librairie Faker pour générer de fausses données aléatoires
        $faker = Factory::create();

        // 
        for ($p = 0; $p < 5; $p++) {
            $individual = new Individual();
            $individual->setEmail($faker->email);
            $individual->setFirstname($faker->firstName);
            $individual->setLastname($faker->lastName);

            // Définition du genre de manière aléatoire
            $randomGender = $faker->randomElement([EnumGender::MALE, EnumGender::FEMALE]);
            $individual->setGender($randomGender);

            $individual->setRoles(['ROLE_USER']);
            $individual->setPassword($this->userPasswordHasherInterface->hashPassword($individual, 'particulier'));

            $this->entityManagerInterface->persist($individual);
        }

        $this->entityManagerInterface->flush();
    }

    /**
     * Create Beekeeper
     *
     * @param ObjectManager $manager
     * @return void
     */
    private function loadBeekeeper(ObjectManager $manager)
    {
        $faker = Factory::create();
        
        for($a = 0; $a < 5; $a++ ) {
            $beekeeper = new Beekeeper();
            $beekeeper->setEmail($faker->email);
            $beekeeper->setFirstname($faker->firstName);
            $beekeeper->setLastname($faker->lastName);

            // Définition du genre de manière aléatoire
            $randomGender = $faker->randomElement([EnumGender::MALE, EnumGender::FEMALE]);
            $beekeeper->setGender($randomGender);

            $beekeeper->setRoles(['ROLE_USER']);
            $beekeeper->setPassword($this->userPasswordHasherInterface->hashPassword($beekeeper, 'apiculteur'));
    
            $this->entityManagerInterface->persist($beekeeper);
        }
        
        $this->entityManagerInterface->flush();
    }
}
