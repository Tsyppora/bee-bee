<?php

namespace App\Enum;

enum EnumGender: string
{
    case FEMALE = "female";
    case MALE = "male";
}