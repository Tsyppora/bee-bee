<?php

namespace App\Controller;

use App\Repository\IndividualRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/individual')]
class IndividualController extends AbstractController
{
    #[Route('/list', name: 'individual_list')]
    public function list(IndividualRepository $individualRepository): Response
    {
        return $this->render('individual/list.html.twig', [
            'individuals' => $individualRepository->findAll(),
        ]);
    }
}
