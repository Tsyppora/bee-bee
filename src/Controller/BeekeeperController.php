<?php

namespace App\Controller;

use App\Repository\BeekeeperRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/beekeeper')]
class BeekeeperController extends AbstractController
{
    #[Route('/list', name: 'beekeeper_list')]
    public function list(BeekeeperRepository $beekeeperRepository): Response
    {
        return $this->render('beekeeper/list.html.twig', [
            'beekeepers' => $beekeeperRepository->findAll(),
        ]);
    }
}
