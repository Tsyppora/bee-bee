<?php

namespace App\Entity;

use App\Repository\BeekeeperRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BeekeeperRepository::class)]
class Beekeeper extends User
{
}
